<?php

namespace App\Controller;

use App\Component\RemoteProvider\Api;
use App\Component\ResultManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rank")
 */
class ResultController extends AbstractController
{

    /**
     * @Route("/response", name="pipeline_check", methods={"GET"})
     */
    public function getResponse(Api $api): JsonResponse
    {
        $result = $api->getData();

        return $this->json($result['data'], $result['status']);
    }

    /**
     * @Route("/init", name="pipeline_get", methods={"GET"})
     */
    public function init(Api $api, ResultManager $manager): JsonResponse
    {
        $response = $api->getData();

        if ($response['status'] === Api::STATUS_ERROR) {
            return $this->json(['error' => 'Not enough incoming data'], Api::STATUS_ERROR);
        }

        $result = $manager->setResultsWithRank($response['data']);

        return $this->json($result);
    }
}
