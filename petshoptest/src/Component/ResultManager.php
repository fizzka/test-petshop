<?php

namespace App\Component;

use App\Entity\Result;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ResultManager
{
    const START_POSITION = 1;

    /** @var SerializerInterface */
    private $serializer;
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    public function setResultsWithRank(array $results): array
    {
        $this->saveResult($results);

        return $this->setRanks();
    }

    private function saveResult(array $results): void
    {
        foreach ($results as $resultData) {
            /** @var Result $result */
            $result = $this->serializer->deserialize(json_encode($resultData), Result::class, 'json', [
                AbstractObjectNormalizer::ALLOW_EXTRA_ATTRIBUTES => false,
                AbstractObjectNormalizer::GROUPS => ['Result.create'],
            ]);
            if ($result instanceof Result) {
                $this->em->persist($result);
            }
        }

        $this->em->flush();
    }

    private function setRanks(): array
    {
        $rank = $count = self::START_POSITION;
        $currentScores = null;

        $results = $this->em->getRepository(Result::class)->findBy([], ['scores' => 'desc']);


        /** @var Result $resultObject */
        foreach ($results as $resultObject) {
            $sameRank = false;

            if ($currentScores === null) {
                $currentScores = $resultObject->getScores();
            } else {
                if ($currentScores === $resultObject->getScores()) {
                    $sameRank = true;
                } else {
                    $currentScores = $resultObject->getScores();
                    $rank = $count;
                }
            }

            if ($sameRank) {
                $resultObject->setRank($rank);
            } else {
                $resultObject->setRank($count);
            }

            $count++;

            $this->em->flush();
        }

        return $results;
    }
}