# Тестовое задание

### Установка и настройка локального окружения ###

- Установить зависимости для симфони:
```
cd petshoptest composer install
```
- Настроить окружение
```
cp .env.dev .env
```

- Запустить контейнеры
```
docker-compose up
```
- Накатить миграции для pg

```
docker-compose exec rest_server php bin/console doctrine:migrations:migrate
```

### Работа приложения ###

По адресу **http://localhost:9090/rank/response** мы видим массив данных с эндпоинта, а по адресу **http://localhost:9090/rank/init** - получаем результат

При каждом следующем **init** данные добавляются в результат и происходит пересчет позиций
